/**
 * Created by hewin on 14-9-3.
 */
exports.web_port=3000;
/*
* web_root可以指定相对目录，也可指定绝对目录，如：public 或 \var\www\xml
* */
exports.web_root='public';
exports.sch_apply_frist_stop=true;
exports.redis={host:'192.168.1.129',port:6380};
exports.pubsub={host:'192.168.1.129',port:6380};
exports.rediswriter={host:'192.168.1.129',port:6380};
exports.database={
    server:"192.168.1.113",
    user:"sa",
    password:"",
    dbname:"gxtest",
    port:"1433"
};