var Tasks=require('maxwin_utils').Tasks;
//var profiler = require('v8-profiler');
var Xmlwriter=require('xml-writer');

Estimatetime=function(){
    this.init=function(){
        this.ext_hander=this.app.ext_hander;
    }
};
function char4(i){
    var s= i.toString();
    if(s.length>=4) return s;
    s='0000'+ i.toString();
    return s.substr(-4,4);
}
Estimatetime.prototype._getEsttime=function(tasks,routeno){
    tasks.addHandler(this,function(routeno){
        this.ext_hander.queryLogicroute(routeno,function(gstops,bstops){
            tasks.response[routeno].gostops=gstops;
            tasks.response[routeno].backstops=bstops;
            tasks.next();
        },{handle_coutdown:true});
    },[routeno]);

};
Estimatetime.prototype._xml=function(data){
    this.res.header('content-type','application/xml; charset=utf-8');
    //var routes=Object.keys(data);
    xml=new Xmlwriter();
    xml.startDocument('1.0', 'UTF-8');
    xml.startElement("BusDynInfo").startElement("EssentialInfo").startElement("Location");
    xml.writeElement("name","Maxwin");
    xml.writeElement("CenterName","");
    xml.writeElement("UpdateTime",(new Date()).datetime2str());//.endElement();
    xml.writeElement("CoordinateSystem","").endElement();
    xml.endElement();
    xml.startElement("BusInfo");
    var keys=Object.keys(data);
    keys.forEach(function(xno){
        var list=data[xno];
        xml.startElement("Route");
        xml.writeAttribute("id",xno.toString());
        list.forEach(function(rs){
            xml.startElement("EstimateTime");
            var keys=["StopID","SID","StopName","GoBack","seqNo","IVRNO","EXTVoiceNo","comeTime","comeCarid","schId","carId","Value"];//Object.keys(rs);
            keys.forEach(function(key){
                xml.startAttribute(key).text(rs[key]==null?'null':rs[key].toString()).endAttribute();
               //xml.writeAttribute(key,rs[key]);
            });
            xml.startElement("ests");
            rs.ests.forEach(function(est){
                xml.startElement("est");
                Object.keys(est).forEach(function(attr){
                    xml.startAttribute(attr).text(est[attr]==null?'null':est[attr].toString()).endAttribute();
                });
                xml.endElement();
            });
            xml.endElement();

            xml.endElement();
        });
        xml.endElement();
    });
   xml.endElement();
    xml.endDocument();
    if(!this.res._header)
        this.res.type('xml');
    return this.res.send(xml.toString());
};
function res_stop(row,lang){

    var re=
    {
    StopID:char4(row.ivrno),
    SID:row.stopid,
    StopName:row.stopname,
    GoBack:row.goback,
    seqNo:row.seqno,
    IVRNO:row.ivrno,
    EXTVoiceNo:row.EXTVoiceNO,// ivrno,
    comeTime:row.cometime,
    comeCarid:row.comecarid,
    schId:row.schs.length>0?row.schs[0].schid:"",
    carId:row.carid,
    Value: row.value==null?"null":row.value,
    ests:row.ests.map(function(est){ delete est.writetime; return est;})
    };//==null && row.schs.length>0 && row.schs[0]==null?-3:row.value;
    switch (lang){
        case "jp":re.StopName=row.jname;break;
        case "en" :re.StopName=row.ename;break;
        default : break;
    }
    return re;
};
/*function Res_stop(row,lang){
    this.row=row;
    this.lang=lang;
}
Res_stop.prototype.__defineGetter__("StopID",function(){return  char4(this.row.ivrno);});
Res_stop.prototype.__defineGetter__("SID",function(){return  this.row.stopid;});
Res_stop.prototype.__defineGetter__("StopName",function(){
    switch (this.lang){
        case "jp":return this.row.jname;break;
        case "en" :return this.row.ename;break;
        default :return this.row.stopname;
            break;
    }
 });
Res_stop.prototype.__defineGetter__("GoBack",function(){return  this.row.goback;});
Res_stop.prototype.__defineGetter__("seqNo",function(){return  this.row.seqno;});
Res_stop.prototype.__defineGetter__("IVRNO",function(){return  this.row.ivrno;});
Res_stop.prototype.__defineGetter__("EXTVoiceNo",function(){return  this.row.ivrno;});
Res_stop.prototype.__defineGetter__("comeTime",function(){return  this.row.cometime;});

Res_stop.prototype.__defineGetter__("comeCarid",function(){return  this.row.comecarid;});
Res_stop.prototype.__defineGetter__("schId",function(){return  this.row.schid;});
Res_stop.prototype.__defineGetter__("carId",function(){return  this.row.carid;});
Res_stop.prototype.__defineGetter__("Value",function(){return  this.row.value==null?"null":this.row.value;});*/

Estimatetime.prototype._by_routeno=function(routeids,view){
//    var snapshot = profiler.takeSnapshot("cpu");
//    profiler.startProfiling("cpu");

    var lang=this.req.query.lang?this.req.query.lang:'zh';
    if(routeids!="")
        routeids=routeids.split(',');
    else
        routeids=[];
    var self=this;
    this.redis.send_command('SMEMBERS',["bd:logicroutes.inuse"],function(err,res){
        if(!err){
            var export_routes=res;
            if(routeids.length==0)
                routeids=export_routes;
            else
                routeids.filter(function(i){return export_routes.some(function(n){return n==i})});
            if(routeids.length==0)
                return sendData({});
            var tasks= new Tasks();
            tasks.response={};
            routeids.forEach(function(xno){
                tasks.response[xno]={gostops:[],backstops:[]};
                self._getEsttime(tasks,xno);
            });
            tasks.nrun(function(){
                var data={};
                routeids.forEach(function(xno){
                    var lr=tasks.response[xno];
                    data[xno]=lr.gostops.concat(lr.backstops).map(function(row){
                        return res_stop(row,lang);//new Res_stop(row,lang);
                    });
                });
                return sendData(data);
            });
        }
        else{
            console.error(err);
            return sendData({});
        }
    });
    function sendData(data){
        if(view=='xml')
            return self._xml(data);
        else{
/*
            self.res.set("Content-Type","application/json; charset=utf-8");
            var keys=Object.keys(data);
            var text="{";
            keys.forEach(function(key){
                if(text.length!=1) text+=",";
                var route=data[key];
                text+="\""+key+"\":[";
                var ls=route.map(function(stop){
                    return JSON.stringify(stop,["StopID","SID","StopName","GoBack","seqNo","IVRNO","EXTVoiceNo","comeTime","comeCarid","schId","carId","Value"]);
                });
                text+=ls.join(",");
                text+="]";
            });
            text+="}";
            self.res.send(text);
*/
           return self.res.json(data);
        }

    }
};

Estimatetime.prototype.byrouteids$xml=function(){
    var routeids=this.req.query.routeids?this.req.query.routeids:"";
    if(routeids==''){
       return this.res.status(403).send("Missing parameter routeids.");
    }else
        return this._by_routeno(routeids,"xml");
};
Estimatetime.prototype.byrouteids$json=function(){
    var routeids=this.req.query.routeids?this.req.query.routeids:"";
    if(routeids==''){
        return this.res.send(403,"Missing parameter routeids.");
    }else
        return this._by_routeno(routeids,"json");
};
Estimatetime.prototype.getall$xml=function(){
    return this._by_routeno("","xml");
};
Estimatetime.prototype.getall$json=function(){
    return this._by_routeno("","json");
};

module.exports.Estimatetime=Estimatetime;
module.exports.README=function(){
/*
 <ul>
     <li>
         <a target="_blank" href="estimatetime/getall.xml?lang=zh">estimatetime/getall.xml?lang=[zh,en,jp]</a>
        <p>得到所有路线的预估时间。返回xml</p>
     </li>
 <li>
 <a  target="_blank" href="estimatetime/getall.json?lang=zh">estimatetime/getall.json?lang=[zh,en,jp]</a>
 <p>得到所有路线的预估时间。返回json</p>
 </li>
 <li>
 <a  target="_blank" href="estimatetime/byrouteids.xml?routeids=179">estimatetime/byrouteids.xml?routeids=85,179&lang=[zh,en,jp]</a>
     <p>得到指定路线的预估时间。返回xml</p>
     </li>
 <li>
 <a  target="_blank" href="estimatetime/byrouteids.json?routeids=179">estimatetime/byrouteids.json？routeids=85,179&lang=[zh,en,jp]</a>
 <p>得到指定路线的预估时间。返回json</p>
 </li>
 </ul>
 <div>
 用于预估时间的输出内容如下：
 <code>
 {"179":[{"StopID":"2001","SID":966,"StopName":"小港站","GoBack":1,"seqNo":1,"IVRNO":2001,"EXTVoiceNo":2001,"comeTime":"15:15","carId":"","comeCarid":"V0015:79039368","Value":null},{"StopID":"2002","SID":967,"StopName":"小港分局","GoBack":1,"seqNo":2,"IVRNO":2002,"EXTVoiceNo":2002,"comeTime":"14:46","carId":"","comeCarid":"V0015:79039367","Value":null},{"StopID":"2503","SID":1723,"StopName":"二苓","GoBack":1,"seqNo":3,"IVRNO":2503,"EXTVoiceNo":2503,"comeTime":"14:47","carId":"","comeCarid":"V0015:79039367","Value":null}}
 </code>
 </div>

 */

};