var Tasks=require('maxwin_utils').Tasks;
var crc32 = require('buffer-crc32');
function Callbus(){

}
Callbus.prototype._dbexec=function(sql,callback){
//    setTimeout(callback,0);
//    return ;
    dbconfig=this.app.get("config").database;
    var Connection = require('tedious').Connection;
    var DbRequest = require('tedious').Request;
    var self=this;
    this.config=
    {
        server:dbconfig.server,
        userName:dbconfig.user,
        password:dbconfig.password,
        options:{
            port:dbconfig.port,
            tdsVersion:"7_1",
            database: dbconfig.dbname,
            debug: {
                packet: true,
                data: true,
                payload: true,
                token: true,
                log: true
            }
        }
    };
    res=[];
    if(!this.connection){
        this.connection = new Connection(this.config);
        this.connection.on('error', function(err) {
            this.connection=null;
            callback(err,res);
        });
        this.connection.on('debug', function(text) {
            // console.log(text);
        });

        this.connection.on('connect',function(err){
            if(err)
                callback(err,res);
            else
                run(self.connection);
        });
    }else
        run(self.connection);
    function run(cnn){
        var request = new DbRequest(sql, function(err) {
            callback(err,res);
//            connection.close();
        });
        request.connection=cnn;
        request.on('error', function(err) {callback(err,res); });
        request.on('row', function(columns) {
            var row={};
            columns.forEach(function(column) {
                row[column.metadata.colName]=column.value;
            });
            res.push(row);
        });
        request.on('done', function(rowCount, more) {
            callback(err,res);
        });
        // In SQL Server 2000 you may need: connection.execSqlBatch(request);
        cnn.execSql(request);
    }
};
Callbus.prototype.alarm$json=function(){
    console.log(JSON.stringify(this.req.query));
    var routeno=parseInt(this.req.query.routeno) || 0;
    var goback=parseInt(this.req.query.goback) || 0;
    var imsi=parseInt(this.req.query.imsi) ||"";
    var self=this;
    if(goback==0)
        return this.res.send({result:0,msg:"unknow goback."});
    if(routeno==0)
        return this.res.send({result:0,msg:"unknow routeno."});
    if(imsi=="")
        return this.res.send({result:0,msg:"unknow imsi."});
    this.app.redis.send_command("get",["bd:estop:"+imsi],(function(err,res){
        if(!err && res!==null){
            var estop=res.seval("bd:estop:"+imsi);
            if(!estop)
                return self.res.send({result:0,msg:"unknow imsi."});
            else
            {
                var stopid=0;
                var len=estop.stops.length;
                for(var i=0;i<len;i++){
                    var rs=estop.stops[i];
                    if(rs.routeno==routeno && rs.goback==goback){
                        stopid=rs.stopid;
                        break;
                    }
                }
                if(stopid!=0)
                    self._callbus(routeno,goback,stopid,'',imsi,(function(re){
                        return self.res.send(re);
                    }).bind(this))
                else{
                    var now=new Date();
                    //var tasks=new Tasks();
                    self.app.redis.send_command('hmget',['bd:logicroute:'+routeno,'name'],function(err,res){
                        var desc;
                        if(!err && res[0]!==null)
                            desc=','+imsi+','+(parseInt(goback)-1)+','+res[0]+','+routeno+',6';
                        else
                            desc=','+imsi+','+(parseInt(goback)-1)+',unknow no: '+routeno+','+routeno+',6';
                        var sql="Pro_InsertDeviceLog 1,'"+imsi+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+desc+"',226";
                        self._dbexec(sql,function(err,res){
                            if(err)
                                console.error(err);
                            sql="Pro_InsertDeviceLog 1,'"+imsi+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+desc+"',227";
                            self._dbexec(sql,function(err,res){
                                if(err)
                                    console.error(err);

                            });

                        });
                    });
                    return self.res.send({result:5,msg:"路線不存在"});
                }
            }
        }
        else
            return self.res.send({result:0,msg:"unknow imsi."});
    }).bind(this));
};
Callbus.prototype.fromweb$json=function(){
    var routeno=parseInt(this.req.query.routeno) || 0;
    var goback=parseInt(this.req.query.goback) || 0;
    var stopid=parseInt(this.req.query.stopid) || 0;
    var memo='web//';
    this.req.ips.forEach(function(ip){
       memo+='->'+ip;
    });
    deviceid=crc32.unsigned(memo+Date.now()).toString(16);
    this._callbus(routeno,goback,stopid,memo,deviceid,(function(re){
        return this.res.send(re);
    }).bind(this));
}
/*
callback(res);
* */


Callbus.prototype._callbus=function(routeno,goback,stopid,memo,deviceid,callback){
/*
    var routeno=parseInt(this.req.query.routeno) || 0;
    var goback=parseInt(this.req.query.goback) || 0;
    var stopid=parseInt(this.req.query.stopid) || 0;
    var from=parseInt(this.req.query.from) || null;
    var memo=this.req.query.memo || this.req.ip;
 uuid=crc32.unsigned(uuid).toString(16);
*/
    var tasks=new Tasks();
    var re={result:0,msg:"",routeno:routeno};
    var carid=null;
    var self=this;
    var stopname=""+stopid;
    var routename=""+routeno;
    //get routename,
    tasks.addHandler(self.app.redis,self.app.redis.send_command,["hmget",["bd:logicroute:"+routeno,"name"],function(err,res){
        if(!err && res !==null)
            routename=res[0] || routename;
        tasks.next();
    }]);
    //以下為測試用，發給特定車機
    tasks.addHandler(self.app.publish,self.app.publish.send_command,["publish",
        ["callbus.FFFF0089",JSON.stringify({routeno:routeno,goback: goback,stopid:stopid,from:""})],
        function(err){
            if(err)
                console.log(err);
            else
                console.log("publish message to FFFF0089");
            tasks.next();
        }
    ]);
    //get stopname
    tasks.addHandler(self.app.redis,self.app.redis.send_command,["hmget",["bd:stop:"+stopid,"name"],function(err,res){
        if(!err && res!==null)
            stopname=res[0] || stopname;
        var now=new Date();
        var desc=routename+ ','+stopname+','+(parseInt(goback)-1)+','+memo;
        var sql="Pro_InsertDeviceLog 1,'"+deviceid+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+desc+"',226";
        tasks.addHandler(self,self._dbexec,[sql,function(err){
            if(err)
                console.error(err);
            tasks.next();
        }])
        tasks.next();
    }]);
    // get esttime
    tasks.addHandler(self.app.redis,self.app.redis.send_command,["get",["est:esttime:"+routeno+":"+goback],
        (function(err,res){
        if(!err && res!=null){
            var obj=res.seval(res);
            if(obj){
               var index= obj.stopid.indexOf(stopid);
               if(obj.value[index]==-3)
                    re.result=4;
               else if(obj.value[index]==null)
                   re.result=3;
               else
                   for(var i=index;i>0;i--){
                       if(obj.ests[i].length>0){ //select car
                           carid=obj.ests[i][0][0];
                           re.carid=carid;
                           re.est=obj.ests[i][0][1];
                           break;
                       }
                   }
            }
        }
        if(carid!=null){
            var body={"routeno":routeno,"goback":goback,"stopid":stopid,"from":""};
            tasks.addHandler(self.app.publish,self.app.publish.send_command,["publish",
                ["callbus."+carid,JSON.stringify(body)],
                (function(err){
                    if(err)
                        re.result=2;
                    else
                        re.result=1;
                    tasks.next();
                }).bind(self)
            ]);
        }
        tasks.next();
    }).bind(self)]);
    tasks.run((function(){
        switch (re.result){
            case 1:re.msg="叫車成功";break;
            case 2:re.msg="失敗";break;
            case 3:re.msg="通訊不良";break;
            case 4:re.msg="無車";break;
            case 5:re.msg="已收班";break;
            case 6:re.msg="路線不存在";break;
        }
        callback(re);
        var now=new Date();
        if(carid)
        memo=memo+"\n車號："+carid;
        var desc=routename+','+stopname+','+(parseInt(goback)-1)+','+memo+",,"+re.result;
//        var sql="Pro_InsertDeviceLog 1,'"+deviceid+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+desc+"',227";
        var sql="Pro_InsertDeviceLog 1,'"+deviceid+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+now.datetime2str()+"','"+desc+"',227";
        self._dbexec(sql,function(err,res){
            if(err)
                console.error(err);

        });
    }).bind(self));
}

module.exports.Callbus=Callbus;
module.exports.README=function(){
/*
<pre>
 1.站機或web或其他gateway送叫車信號到realtime_web,
 a. 多媒體站機叫車： callbus/alarm.json?routeno=179&goback=1&imsi=134
 b. web叫車： callbus/fromweb.json?routeno=179&goback=1&stopid=134
 c . 其他方式叫車 callbus/from.json?routeno=179&goback=1&stopid=134&from=[2]
 from目前定義為：【0："按鈕",1："刷卡",2："WEB"】
 </pre>
 <p>web回應為：</p>
 <code>
 {"result":1,"msg":""}
 </code>
 result: 0: "unknow",1:"叫車成功",2:"失敗",3:"通迅不良",4:"無車",5:"已收班",6:"路線不存在"
*/

}