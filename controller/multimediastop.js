var Tasks=require('maxwin_utils').Tasks;
var Xmlwriter=require('xml-writer');
function Multimediastop(){
    this.init=function(){
        this.ext_hander=this.app.ext_hander;
    };
    this.getest_by_imsi=function(imsi,callback){
        var self=this;
        this.redis.send_command('get',["bd:estop:"+imsi],function(err,res){
            var estop=null;
            if(!err && typeof res=="string"){
               estop=res.seval();
            }
            if(estop==null)
                callback(null);
            else{
                tasks=new Tasks();
                var step3= new Tasks();
                for(var i=0;i<estop.stops.length;i++){
                    tasks.addHandler(self,function(index){
                        var rs=estop.stops[index];
                        self.ext_hander.queryStop(rs.routeno,rs.goback,rs.stopid,function(stop){
                            if(stop===null)
                                return estop.stops.splice(index,1);
                            estop.stops[index].cometime=stop.schs.length>0? (new Date(stop.schs[0].time*1000)).makeHM():"";
                            estop.stops[index].comecarid=stop.schs.length>0?stop.schs[0].carid:"";
                           estop.stops[index].hasschedule=stop.schs.length>0?"1":"0";//todo: ??
                            estop.stops[index].ests=stop.ests;
                            estop.stops[index].value=stop.value;
                            estop.stops[index].carid=stop.carid;
                            estop.stops[index].ests.forEach(function(est){
                                if(est.carid=="") return;
                                est.stopname="";
                                step3.addHandler(self,function(est){//得到当前站
                                    self.redis.send_command('hmget',["bd:vehicle:"+est.carid,"cstop_name"],function(err,res){
                                        if(!err)
                                            est.stopname=res[0];
                                        step3.next();
                                    });
                                },[est])
                            });
                            tasks.next();
                        },{handle_coutdown:true});
                    },[i]);
                }
                tasks.nrun(function(){
                    step3.nrun(function(){
                        self._writeEstopState(imsi,estop);
                        callback(estop);
                    });
                });
            }
        });
    }
}
Multimediastop.prototype._writeEstopState=function(imsi,estop,ads){
    var est= estop.stops.map(function(s){
        return s.routename+'['+ (s.goback==1?'去程':'回程')+']:'+ s.value;
    })
//    var adv=ads.map(function(ad){
//        return ad
//    });
    var arr=['estop:statusinfo:'+imsi];
    arr.push('est');
    arr.push(JSON.stringify(est));
    arr.push('alivetime');
    arr.push(Date.linux_now());
//    arr.push('est');
//    arr.push('est');
    this.app.rediswriter.send_command('hmset',arr,function(){});
    this.app.rediswriter.send_command('zadd',['estop:infochange',Date.linux_now(),imsi],function(){});
}
Multimediastop.prototype.getmstopest$xml=function(){
    var imsi=this.req.query.id?this.req.query.id:null;
    if(imsi==null)
        return this.res.send("Missing parameter id.");
    var self=this;
    this.getest_by_imsi(imsi,function(estop){
        //此方法只需支持xml
        xml=new Xmlwriter();
        xml.startDocument('1.0', 'UTF-8');
        xml.startElement("Est").startElement("MediaStop");
        //<MediaStop ID="30001000" name="金獅湖站">
        xml.writeAttribute("ID",imsi);
        function writeAtt(name,value){
            xml.startAttribute(name).text(value).endAttribute();
        }
        if(estop!=null){
            xml.writeAttribute("name",estop.name);
            estop.stops.forEach(function(rs){
                xml.startElement("Trip");
                //<Trip RouteNo="207" GoBack="1" stop="金獅湖站" desc="金獅湖站->龍華國小" from="金獅湖站"
                // to="捷運凹子底站" voiceID="5000" location="" comeTime="09:55"></Trip>

                writeAtt("RouteNo",rs.routeno);
                writeAtt("GoBack",rs.goback);
                writeAtt("stop",rs.stopname);
                writeAtt("desc",rs._desc);
                writeAtt("from",rs.from);
                writeAtt("to",rs.to);
                writeAtt("voiceID",rs.voiceid);
                writeAtt("location",rs.site);
                writeAtt("comeTime",rs.cometime);
                rs.ests.forEach(function(est){
                    xml.startElement("stop");
                    writeAtt("name",est.stopname);
                    writeAtt("Time",est.est);
                    writeAtt("carId",est.carid);
                    xml.endElement();
                });
                xml.endElement();
            });
        }
        xml.endElement();
        xml.endElement();
        xml.endDocument();
        return self.res.type('xml').send(xml.toString());
    });
}
Multimediastop.prototype.getdata$json=function(){
    //this.res.send({});return
    var imsi=this.req.query.imsi?this.req.query.imsi:null;
    if(imsi==null)
        return this.res.send("Missing parameter imsi.");
    var self=this;
    this.getest_by_imsi(imsi,function(estop){
        var re={};
        if(estop!=null){
            re.name=estop.name;;
            re.eName=estop.ename;
            re.code=estop.code;
            re.stype=estop.stype;
            re.stopNO=imsi;
            re.displaymode=estop.displaymode;
            re.VTID=estop.vtid;
            //re.ad="";
            var now=Date.linux_now();
            estop.ads=estop.ads.filter(function(ad){
                return (now>=ad.playstarttime && now<ad.playendtime);
            });
            re.ad= estop.ads.map(function(ad){return ad.content}).join("\n\r");
           // re.routestops=[];
            var tasks=new Tasks();
            re.routestops=estop.stops.map(function(rs){
                var stop={};
                stop.routeName=rs.routename;
                stop.stopname=rs.stopname;
                stop.linexno=rs.linexno;
                stop.from=rs.from;
                stop.to=rs.to;
                stop.eFrom=rs.efrom;
                stop.eTo=rs.eto;
                stop.voiceID=rs.voiceid;
                stop.eRoutename=rs.eroutename;
                stop.routeNo=rs.routeno.toString();
                stop.goback=rs.goback.toString();
                stop.stopid=rs.stopid.toString();
                stop.site=rs.site;
                stop._desc=rs._desc;
                stop._eDesc=rs._edesc;
                stop.direction=rs.to;
                stop.comeTime=rs.cometime;
                stop.comeCarid=rs.comecarid;
                stop.hasSchedule=rs.hasschedule;//.length>0?"1":"0";
                stop.comeType="";
                stop.est=rs.value;//>=0?rs.value.toString():null;
                stop.carId=rs.carid;
                stop.carType="";
                if(stop.carId!="")
                    tasks.addHandler(stop,function(carid){
                        self.redis.send_command('hmget',["bd:vehicle:"+carid,"iconref"],(function(err,res){
                            if(!err && res[0])
                                this.carType=res[0].toString();
                            tasks.next();
                        }).bind(this));
                    },[stop.carId]);
                if(stop.comeCarid!="")
                    tasks.addHandler(stop,function(carid){
                        self.redis.send_command('hmget',["bd:vehicle:"+carid,"iconref"],(function(err,res){
                            if(!err && res[0])
                                this.comeType=res[0].toString();
                            tasks.next();
                        }).bind(this));
                    },[stop.comeCarid]);
                return stop;
//                .push(stop);
            });
            tasks.nrun(function(){
                try
                {
                    return self.res.status(200).send(re);
                }catch (e)
                {
                    return console.error((new Date()).datetime2str(),e,self.req.query);
                }
            });
        }else
           return self.res.send(re);
        });
}
module.exports.Multimediastop=Multimediastop;
module.exports.README=function(){
    /*
     <ul>
     <li>
     <a  target="_blank" href="multimediastop/getdata.json?imsi=112233445522222">multimediastop/getdata.json?imsi=IMSI</a>
     <p> 此URI只支持json返回</p>
     <div>
         用于预估时间的输出,指定路线号列表，或不指routeids输出所有线路的预估时间。
        <code>
     {"code":"E0002","eName":"dayebeilu","name":"大業北路","routestops":[{"_desc":"公車小港站->鳳鼻頭","_eDesc":"Fongbitou","direction":"鳳鼻頭","eRoutename":"Red2 ","goback":1,"id":"17901_1_1724","routeName":"RED-01","routeNo":17901,"site":"","stopid":"1724"}],"ad":""}
          </code>
     </div>
     </li>

     <li>
     <a  target="_blank" href="multimediastop/getmstopest.xml?id=112233445522222">multimediastop/getmstopest.xml?id=IMSI</a>
     <p> 此URI只支持xml返回</p>
     <div>
     用于预估时间的输出,指定路线号列表，或不指routeids输出所有线路的预估时间。
     <code>
        <per>
         <Est>
         <MediaStop ID="112233445512345" name="大業北路">
         <Trip RouteNo="17901" GoBack="1" stop="大業北路" desc="公車小港站->鳳鼻頭" from="公車小港站" to="鳳鼻頭" voiceID="2504"/>
         </MediaStop>
         </Est>

     <per>
    </code>
     </div>
     </li>
     </ul>
     */

}