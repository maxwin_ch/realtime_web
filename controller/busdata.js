/**
 * Created by hewin on 14-9-4.
 */
var Tasks=require('maxwin_utils').Tasks;
var Xmlwriter=require('xml-writer');
//var Est_time=require('maxwin_est_time').Est_time;

Busdata=function(){

};
Busdata.prototype._handle_cars=function(cars,getevent,callback){
    //var requestcount=0;
    var res_cars=[];
    var events=[];
    var self=this;
    var tasks=new Tasks();
    cars.forEach(function(car){
//        if(!car)
//            return;
        var bus={};
        bus.busstatus=parseInt(car.busstatus);
        bus.carno=car.carid;
        bus.caronstop=car.caronstop;
        bus.connectstate=parseInt(car.connectstate);
        bus.currstopname=car.cstop_name;
        bus.drivename='';
        bus.drivertel='';
        bus.engstopname='';
        bus.dutystatus=parseInt(car.dutystatus);
        bus.fleetid=car.fleetid;
        bus.goback=parseInt(car.goback);
        bus.gps_cause=car.course;//car.course;
        // bus.gps_dir=car.course;
        bus.gps_time=car.gpstime?(new Date(parseInt(car.gpstime)*1000).datetime2str()):null;
        // bus.hasload=0;
        bus.lastmodify=car.gpstime;
        bus.lat=parseFloat(car.y);
        //  bus.lat.toString=function(){return this.toFixed(6)}
        bus.lon=parseFloat(car.x);
        // bus.lon.toString=function(){return this.toFixed(6)};
        bus.nextstopname=car.nstop_name;
        bus.priorstopname=car.pstop_name;
        bus.ps_boardingcount=0;
        bus.ps_debarkationcount=0;
        bus.ps_totalcustcount=0;
        // bus.toendstopcount=0;
        bus.routename='';
        bus.routeno=parseInt(car.routeno);
        bus.speed=parseInt(car.speed);
        bus.city=car.city?car.city:"";
        bus.town=car.town?car.town:"";
        bus.street=car.street?car.street:"";
        bus.vehicle_type=car.vehicletypeid;
        res_cars.push(bus);
        if(bus.routeno){
            tasks.addHandler(self,function(bus){
                self.redis.send_command('hmget',['bd:logicroute:'+bus.routeno,'name'],(function(err,res){
                    if(!err)
                        this.routename=res[0];
                    tasks.next();
                }).bind(bus));
            },[bus])
        }
        if(getevent)
            tasks.addHandler(self,function(carid){
                this.redis.send_command('hgetall',["est:vehicle.event:"+carid],function(err,res){
                try{
                    if(!err && res){
                        var keys=Object.keys(res);
                        var now=Date.linux_now();
                        keys.forEach(function(key){

                            //var code=key;
                            var event=JSON.parse(res[key]);
                            if(event){
                                event.writed=parseInt(event.writed);
                                if((now-event.writed)<24*60*60)//大于一天
                                {
                                    event.code=code;
                                    event.carno=carid;
                                    delete(event.writed);
                                    if((event.etime+60)<Date.linux_now())
                                        event.value='';
                                    events.push(event);
                                }
                            }
                        });
                    }
                }catch(e){
                   console.error(e);
                }
                tasks.next();
            });
        },[bus.carno]);
    });
    tasks.nrun(function(){
        callback(res_cars,events);
    });
};
Busdata.prototype._get_by_carids=function(carids,callback){
    var res_cars=[];
    var self=this;
    tasks=new Tasks();
    carids.forEach(function(carid){
        tasks.addHandler(self,function(carid){
            self.redis.send_command('hgetall',['bd:vehicle:'+carid],(function(err,res){
                if(res && res.carid)
                    res_cars.push(res);
                tasks.next();
            }));
        },[carid])
    });
    tasks.nrun(function(){
        callback(res_cars);
    });
};
Busdata.prototype._bycarids=function(view){
    if(!this.req.query.carids)
        return this.res.status(403).send("Missing parameter carids.");
    else{
        var carids=this.req.query.carids.split(',');
        return this._getbusdata(carids,view);
    }
};
Busdata.prototype._byrouteids=function(view){
    var self=this;
    if(this.req.query.routeids){
        var routeids=this.req.query.routeids.split(',');
        if(routeids.length==0)
            this._getbusdata([],view);
        else
        {
            var tasks=new Tasks();
            var time=Date.linux_now()-180;
            var carids=[];
            routeids.forEach(function(routeno){
                tasks.addHandler(self,function(routeno){
                    this.redis.send_command('ZRANGEBYSCORE',['est:online.cars:'+routeno,time,'+inf'],function(err,res){
                        if(!err)
                            carids=carids.concat(res);
                        tasks.next();
                    });
                },[routeno]);
            });
            tasks.nrun(function(){
                self._getbusdata(carids,view,routeids);
            });
        }

    }else
        this.res.status(403).send("Missing parameter routeids.");
};
Busdata.prototype._getall=function(view){
    var self=this;
    this.redis.send_command('SMEMBERS',['bd:vehicles'],function(err,res){
        if(err)
           return self._getbusdata([],view);
        else
           return self._getbusdata(res,view);
    });

};
Busdata.prototype._busdata2xml=function(cars){
    xml=new Xmlwriter();
    xml.startDocument('1.0', 'UTF-8');
    xml.startElement("BusDynInfo").startElement("EssentialInfo").startElement("Location");
    xml.writeElement("name","Maxwin");
    xml.writeElement("CenterName","");
    xml.writeElement("UpdateTime", (new Date()).datetime2str());//.endElement();
    xml.writeElement("CoordinateSystem","").endElement();
    xml.endElement();
    xml.startElement("BusInfo");
    cars.forEach(function(car){
        xml.startElement("BusData");
        var keys=Object.keys(car);
        keys.forEach(function(key){
            xml.startAttribute(key).text(car[key]).endAttribute();
        });
        xml.endElement();
    });
    xml.endElement();
    xml.endDocument();
    return xml.toString();
};
Busdata.prototype._getbusdata=function(carids,view,routeids){
   var self=this;
    self._get_by_carids(carids,function(cars){
        var reslist=[];
        cars.forEach(function(car){
            var bus={};
//            BusOnStop :='0';
//            DutyStatus :='90';
//            BusStatus :='90';
            if(!car.fleetid) return ;
            if(routeids && !routeids.some(function(routeid){ return routeid==car.routeno}))
                return;
            bus.BusID=car.carid;
            bus.ProviderID=car.providerid?car.providerid.toString():"";
            bus.DutyStatus=car.dutystatus?car.dutystatus.toString():"90";
            bus.BusStatus=car.busstatus?car.busstatus.toString():"90";
            bus.RouteID=car.routeno?car.routeno.toString():"";
            bus.GoBack=car.goback?car.goback.toString():"";
            bus.Longitude=car.x?car.x.toString():"";
            bus.Latitude=car.y?car.y.toString():"";
            bus.Speed=car.speed?car.speed.toString():"0";
            bus.Azimuth=car.course?car.course.toString():"0";
            bus.DataTime=car.gpstime?(new Date(parseInt(car.gpstime)*1000).datetime2str()):"";
            reslist.push(bus);
        });
        return doresponse(reslist);
      //  self._handle_cars(cars,false,doresponse);
    });
    function doresponse(cars){
        switch (view){
            case 'xml':
                var text=self._busdata2xml(cars);
                return self.res.status(200).type('xml').send(text);
            break;
            case "json":
                return self.res.status(200).send(cars);
                break;
            default :return self.res.status(404).send("not find view.");
        }
    }
};
//得到变化的车辆
Busdata.prototype._getmodifycarsbyfleetid=function(fleetid,min,max,callback){
    if(min==0)//车队下的所有车
    {
        this.app.redis.send_command('SMEMBERS',['bd:fleet.vehicles:'+fleetid],(function(err,res){
            if(err || res==null)
                callback([]);
            else
                callback(res);
        }));
    }else
        this.app.redis.send_command('ZRANGEBYSCORE',['est:fleet.modify.vehicles:'+fleetid,min,max],(function(err,res){
            if(!err){
               callback(res);
            }else
                callback([]);
        }));
};
Busdata.prototype.getall$json=function(){
    this._getall("json");
};
Busdata.prototype.getall$xml=function(){
    this._getall("xml");
};
Busdata.prototype.bycarids$json=function(){
    this._bycarids("json");
};
Busdata.prototype.bycarids$xml=function(){
    this._bycarids("xml");
};
Busdata.prototype.byrouteids$json=function(){
    this._byrouteids("json");
};
Busdata.prototype.byrouteids$xml=function(){
    this._byrouteids("xml");
};
Busdata.prototype.writecookei=function(){
    if(!this.req.query.name || !this.req.query.value)
        return this.res.status(403).send("Missing parameter \"name\" \"value\".");
    return this.res.status(200).header("Set-Cookie",this.req.query.name+"="+this.req.query.value+";path=/;").send("OK");
};

Busdata.prototype.byrangs$json=function(){
    var rangs=this.req.query.rangs?this.req.query.rangs:"";
    rangs=rangs.split(',');
    if(rangs.length==0 || (rangs.length % 4) !=0)
        return this.res.status(403).send("Missing parameter \"rangs\".");
    var ranglist=[];
    rangs=rangs.map(function(s){return parseFloat(s);});
    for(var i=0;i<(rangs.length/4);i++){
        ranglist.push({minx:rangs[i*4],miny:rangs[i*4+1],maxx:rangs[i*4+2],maxy:rangs[i*4+3]});
    }
    if(!this.req.cookies.user)
        return this.res.status(403).send("没有权限，please login.");
    self=this;
    this.redis.send_command('hmget',["bd:accredituser:"+this.req.cookies.user,"fleets"],function(err,res){
        if(err || res==null){
            console.error(err,res);
            return self.res.send([]);
        }else
        {
            var fleets=res[0].split(',');
            var carids=[];
            var tasks=new Tasks();
            fleets.forEach(function(fleetid){
                if(fleetid!='')
                    tasks.addHandler(self,self._getmodifycarsbyfleetid,[fleetid,0,Date.linux_now(),function(res){
                        carids=carids.concat(res);
                        tasks.next();
                    }]);
            });
            tasks.nrun(function(){
                self._get_by_carids(carids,function(cars){
                    self._handle_cars(cars,true,function(res_cars,events){
                        res_cars=res_cars.filter(function(car){
                            return ranglist.some(function(r){
                                return (car.lon && car.lat && (car.lon>= r.minx)&&(car.lon<= r.maxx)&&(car.lat> r.miny)&&(car.lat<= r.maxy));
                            });
                        });
                        events=events.filter(function(event){
                            return res_cars.some(function(car){
                                return car.carno==event.carno;
                            });
                        });
                        var re={success:true,time:Date.linux_now(),vehicles:{data:res_cars},events:events};
                        return self.res.send(re);
                    });
                });

            });
        }
    });
};

Busdata.prototype.user_all$json=function(){
    if(!this.req.cookies.user)
        return this.res.status(403).send("没有权限，please login.");
    var self=this;
    this.redis.send_command('hmget',["bd:accredituser:"+self.req.cookies.user,"fleets"],function(err,res){
        var fleets=[];
        if(!err && res!==null)
            fleets=res[0].split(',');
        var lasttime=self.req.query.modifiedsince?parseInt(self.req.query.modifiedsince):0;// this.req.cookies.lasttime?this.req.cookies.lasttime:"{}";
        var nexttime=Date.linux_now();
        var tasks=new Tasks();
        var res={success:true,time:nexttime,vehicles:{data:[]},events:[]};
        fleets.forEach(function(fleetid){
            tasks.addHandler(self,self._getmodifycarsbyfleetid,[
                fleetid,lasttime,nexttime,(function(carids){
                    self._get_by_carids(carids,(function(cars){
                        self._handle_cars(cars,true,(function(res_cars,events){
                            res.vehicles.data=res.vehicles.data.concat(res_cars);
                            res.events=res.events.concat(events);
                            tasks.next();
                        }).bind(self));
                    }).bind(self));
                }).bind(self)
            ]);
        });
        tasks.run((function(){
            self.res.json(res);
        }).bind(self));
    });
};
Busdata.prototype.byfleet$json=function(){
    var fleetid=this.req.query.fleetid;
    var lasttime=this.req.query.modifiedsince?parseInt(this.req.query.modifiedsince):0;// this.req.cookies.lasttime?this.req.cookies.lasttime:"{}";
    var nexttime=Date.linux_now();
    this._getmodifycarsbyfleetid(fleetid,lasttime,nexttime,(function(carids){
        this._get_by_carids(carids,(function(cars){
            this._handle_cars(cars,true,(function(res_cars,events){
                var re={success:true,time:nexttime,vehicles:{data:res_cars},events:events};
                try
                {
                    this.res.json(re);
                }catch (e)
                {
                    console.error((new Date()).datetime2str(),e,fleetid,lasttime);
                }
            }).bind(this));
        }).bind(this));
    }).bind(this));
};
Busdata.prototype.for_stop$json=function(){
    var route_no=this.req.query.route_no;
    var goback=this.req.query.goback;
    var stopid=this.req.query.stop_code;
    var tasks=new Tasks();
    var self=this;
    tasks.addHandler(this,function(route_no){
       this.app.redis.send_command('hmget',['bd:logicroute:'+route_no,'mergebackest'],function(err,res){
            if(err)
                tasks.mergebackest=false;
            else
                tasks.mergebackest=res[0]!=0;
           tasks.next();
       });
    },[route_no])
    tasks.addHandler(this,function(route_no){
        this.app.redis.send_command('ZRANGEBYSCORE',['est:online.cars:'+route_no,Date.linux_now()-300,'+inf'],(function(err,res){
            if(!err){
                var cars=res;
                this._get_by_carids(cars,(function(cars){
                    this._handle_cars(cars,false,(function(res_cars,events){
                        tasks.cars=res_cars;
                        if(!tasks.mergebackest || goback==1 )
                            tasks.cars=tasks.cars.filter(function(car){
                                return car.goback==goback;
                            });
                        tasks.cars=tasks.cars.filter(function(car){
                            return car.connectstate==true;
                        });
                       // tasks.events=events;
                        tasks.next();
                    }).bind(this));

                }).bind(this))
            }else
                tasks.next();
        }).bind(this));

    },[route_no]);
    tasks.addHandler(this,function(route_no,goback,stopid){
        self.app.ext_hander.queryRoute(route_no,goback,function(stops){
            // var est={carno:"",minesttime:null, stopcode: sid};
            tasks.stopminest=[];//ests;
            tasks.onestopest=[{estimateslist:[]}];
            stops.forEach(function(stop){
                var est={carno:stop.carid,minesttime:stop.value>=0?stop.value:null, stopcode: stop.stopid};
               if(stop.goback==goback && stop.stopid==stopid)
                    stop.ests.forEach(function(est){
                        tasks.onestopest[0].estimateslist.push({carno:est.carid,est:est.est});
                    });
                   onestopest=
                tasks.stopminest.push(est);
            });
            tasks.next();
        },{handle_coutdown:true})
    },[route_no,goback,stopid]);

    tasks.run((function(){
        var re={success: true, data: {
            vehicle_stat:tasks.cars,
            stopminest:tasks.stopminest,
            onestopest:tasks.onestopest,
            events:tasks.events
        }};
        this.res.json(re);
    }).bind(this));
};
exports.Busdata=Busdata;
exports.README=function(){
/*
 <ul>
 <li>
 <a  target="_blank" href="busdata/bycarids.json?carids=V0015,V0005">busdata/bycarids.json?carids=V0015,V0005</a>
 <p>得到指定车牌号的车状态。返回json</p>
 </li>
 <li>
 <a  target="_blank" href="busdata/bycarids.xml?carids=V0015,V0005">busdata/bycarids.xml?carids=V0015,V0005</a>
 <p>得到指定车牌号的车状态。返回xml</p>
 </li>
 <li>
 <a  target="_blank" href="busdata/getall.json">busdata/getall.json</a>
 <p>得到所有车状态。返回json</p>
 </li>
 <li>
 <a  target="_blank" href="busdata/getall.xml">busdata/getall.xml</a>
 <p>得到所有车状态。返回jxml</p>
 </li>

 <li>
 <a  target="_blank" href="busdata/byrouteids.xml?routeids=179"> busdata/byrouteids.xml?routeids=179</a>
 <p>得到指定路线上的车状态。返回xml</p>
 </li>
 <li>
<a  target="_blank" href="busdata/byrouteids.json?routeids=179"> busdata/byrouteids.json?routeids=179</a>
 <p>得到指定路线上的车状态。返回json</p>
 </li>
 <li>
<a  target="_blank" href="busdata/byfleet.json?fleetid=10022"&modifiedsince=0> busdata/byfleet.json?fleetid=10022[&modifiedsince=]</a>
 <p>得到指定车队的车状态。返回json.modifiedsince参数可选，只取修改的数据.modifiedsince的值是上次回应中的time</p>
 </li>
 <li>
<a  target="_blank" href="busdata/for_stop.json?route_no=179&goback=1&stop_code=966"> busdata/for_stop.json?route_no=179&goback=1&stop_code=966</a>
 <p>得到指定路线的车状态和预估时间，供webmonitor使用。返回json</p>
 </li>
 <li>
 <a  target="_blank" href="busdata/byrangs.json?rangs=0,0,180,90"> busdata/byrangs.json?rangs=0,0,180,90</a>
 <p>得到指定范围内的车辆，供webmonitor使用。<br>
 参数可指定多个范围逗号分隔，如:<br>
 rangs=minx1,miny1,maxx1,maxy2...minxn,minyn,maxxn,maxyn
 <br>
 返回json</p>
 </li>
 <li>
 <a  target="_blank" href="busdata/user_all.json"> busdata/user_all.json[modifiedsince=]</a>
 <p>用户下所有车队的车状态。返回json.modifiedsince参数可选，只取修改的数据.modifiedsince的值是上次回应中的time</p>
  <p>
 返回json</p>
 </li>

 <p>
 返回JSON:
<code>
 {"success":true,"time":1416452365,
 "vehicles":{"data":[
 {"busstatus":98,"carno":"V0020",
 "connectstate":0,"drivename":"","drivertel":"","engstopname":"",
 "dutystatus":0,"fleetid":"10013","goback":0,"gps_cause":"152",
 "gps_time":"2014-11-19 10:46:40","lastmodify":"1416365200",
 "lat":22.642997,"lon":120.326599,"ps_boardingcount":0,
 "ps_debarkationcount":0,"ps_totalcustcount":0,"routename":"red2_7",
 "routeno":8807,"speed":6,"city":"","town":"高雄市三民區","street":"大順二路",
 "vehicle_type":"100005"}]},

 "events":[]}
</code>
</p>
 </ul>
*/
};