var path=require('path');
var fs=require('fs');
var os=require('os');
var styles = {
    'bold'          : ['\x1B[1m',  '\x1B[22m'],
    'italic'        : ['\x1B[3m',  '\x1B[23m'],
    'underline'     : ['\x1B[4m',  '\x1B[24m'],
    'inverse'       : ['\x1B[7m',  '\x1B[27m'],
    'strikethrough' : ['\x1B[9m',  '\x1B[29m'],
    'white'         : ['\x1B[37m', '\x1B[39m'],
    'grey'          : ['\x1B[90m', '\x1B[39m'],
    'black'         : ['\x1B[30m', '\x1B[39m'],
    'blue'          : ['\x1B[34m', '\x1B[39m'],
    'cyan'          : ['\x1B[36m', '\x1B[39m'],
    'green'         : ['\x1B[32m', '\x1B[39m'],
    'magenta'       : ['\x1B[35m', '\x1B[39m'],
    'red'           : ['\x1B[31m', '\x1B[39m'],
    'yellow'        : ['\x1B[33m', '\x1B[39m'],
    'whiteBG'       : ['\x1B[47m', '\x1B[49m'],
    'greyBG'        : ['\x1B[49;5;8m', '\x1B[49m'],
    'blackBG'       : ['\x1B[40m', '\x1B[49m'],
    'blueBG'        : ['\x1B[44m', '\x1B[49m'],
    'cyanBG'        : ['\x1B[46m', '\x1B[49m'],
    'greenBG'       : ['\x1B[42m', '\x1B[49m'],
    'magentaBG'     : ['\x1B[45m', '\x1B[49m'],
    'redBG'         : ['\x1B[41m', '\x1B[49m'],
    'yellowBG'      : ['\x1B[43m', '\x1B[49m']
};

var mkdirs = function(dirpath, mode, callback) {
    fs.exists(dirpath, function(exists) {
        if(exists) {
            callback && callback(dirpath);
        } else {
            //尝试创建父目录，然后再创建当前目录
            mkdirs(path.dirname(dirpath), mode, function(){
                fs.mkdir(dirpath, mode, callback);
            });
        }
    });
};
function print_infi(){
    //if(os.type().toLocaleLowerCase()=='linux'){
        var configdir='/etc/maxwin/realtime_web';
        mkdirs('/etc/maxwin/realtime_web',777);
    console.log(fs.readdirSync(configdir));
        if(fs.readdirSync(configdir).length==0){
            var input = fs.createReadStream(__dirname+'/config.sample.js');
            var output = fs.createWriteStream('/etc/maxwin/realtime_web/config.js');
            console.log(__dirname+'/config.sample.js','/etc/maxwin/realtime_web/config.js');
            input.pipe(output);
        }
    //}
    console.log("");
    console.log("**********************************************************\n");
    //console.log(styles['bold'].join(''));
    console.log(styles.bold.join('請檢查配置文件.\n參考樣本配置：'+ __dirname+'/config.sample.js\n'));
    console.log(styles.grey.join('mkdir /etc/maxwin & mkdir /etc/maxwin/realtime_web & cp '+ __dirname+'/config.sample.js /etc/maxwin/realtime_web/config.js'));
    console.log(styles.green.join("node啟動：\n"));
    console.log("node "+__dirname+" config=/etc/maxwin/realtime_web/config.js\n");
    console.log(styles.green.join("pm2啟動：\n"));
    console.log("pm2 start "+__dirname+" -- config=/etc/maxwin/realtime_web/config.js\n");
    console.log("**********************************************************\n");
    console.log("");
}
setTimeout(print_infi,0);