var Tasks=require('maxwin_utils').Tasks;
Est_hander=function(redis,pubsub){
    this.redis=redis;
    this.pubsub=pubsub;
    this.pubsub.on('connect',this.subscribe.bind(this));
    this.pubsub.on('pmessage',this.onmessage.bind(this));
    this.pubsub.on('error',function(err){
        console.error((new Date()).datetime2str(),'subscriber ',err);
    });
    this.routes={};
};
Est_hander.sch_apply_frist_stop=false;
Est_hander.prototype.onmessage=function(channel, message){
    if(channel=="bdupd.logicroute.*"){
        var keys=message.split('.');
        var routeno= keys.pop();
        delete(this.routes[routeno+"_1"]);
        delete(this.routes[routeno+"_2"]);
    }
    if(message=="bdupd.basedata.recreateall")
        this.routes={};
};
Est_hander.prototype.subscribe=function(){
    this.pubsub.send_command('psubscribe',["bdupd.logicroute.*","bdupd.basedata.recreateall"],function(err,value){
        if(err){
            console.warn((new Date()).datetime2str(),err,'subscribe  retry at 10 Sec.');
            setTimeout(self.subscribe.bind(self),10000);
        }
        else
            console.log((new Date()).datetime2str(),'psubscribed',value);
    });
};
//Est_hander.prototype.decode_est=function(s){
//    var res= s.seval();
//    if(res==null) return [];
//    var list=[];
//    var len=res.stopid.length;
//    return res.stopid.map(function(stopid,i){
//        var ests= res.ests[i].map(function(est){//[est.carid,est.est,est.countdowntime,est.writetime]
//            return {carid:est[0],est:est[1],countdowntime:est[2],writetime:est[3]};
//        });
//        var schs= res.schs[i].map(function(sch){//{"carid":"V0011","schid":"79339577","time":1417768689,"islast":true}
//            return {carid:sch[0],schid:sch[1],time:sch[2],islast:sch[3]};
//        });
//        stop={
//            stopid:res.stopid[i],
//            value:res.value[i],
//            carid:res.carid[i],
//            comecarid:res.comecarid[i],
//            cometime:res.cometime[i],
//            ests:ests,
//            schs:schs
//        };
//        return stop;
//    });
//
//}
function Est(list,states){
    this.carid=list[0];
    var car=states[this.carid];
    this.est=list[1]>0?Math.ceil(list[1]/60):list[1];
    this.countdowntime=car?car.countdowntime : 0;
    this.writetime=car?car.writetime : 0;
    this.islast=car?car.islast : 0;
}
function Sch(list){
    this.carid=list[0];
    this.schid=list[1];
    this.time=list[2];
    this.islast=list[3];
}
Est_hander.prototype.queryRoute=function(routeno,goback,callback){
    var tasks=new Tasks();
    var eststops=null;
    var self=this;
    var dbstops=self.routes[routeno+'_'+goback] || null;
    if(!dbstops)
        tasks.addHandler(self,function(routeno,goback){
            self.redis.send_command("get",["bd:allstops:"+routeno+":"+goback],function(err,res){
                    if(!err && res!=null){
                        dbstops=res.seval("bd:allstops:"+routeno+":"+goback)||[];
                        var seqNo=0;
                        dbstops=dbstops.filter(function(stop){
                            // rewrite seqNo 2015-5-5
                            if(stop.stoptype!=2)
                            {
                                stop.seqno=++seqNo;
                                return true;
                            }else
                                return false;
                        });

                        self.routes[routeno+'_'+goback]=dbstops;
                    }
                    tasks.next();
                });
        },[routeno,goback]);
    tasks.addHandler(self,function(routeno,goback){
        self.redis.send_command("get",["est:esttime:"+routeno+":"+goback],function(err,res){
            if(!err && res!==null)
                eststops=res.seval("est:esttime:"+routeno+":"+goback);//self.decode_est(res) ;
            tasks.next();
        });
    },[routeno,goback]);
    function mager(dbstops,eststops,schs,stop1,index,schstate){
        if(dbstops===null) return [];
        if(eststops===null || dbstops.length!=eststops.stopid.length){
            dbstops=dbstops.map(function(stop){
                stop.ests=[];stop.schs=[];
                stop.cometime="";
                stop.comecarid="";
                stop.carid="";
                stop.value=null;
                return stop;
            });
            return dbstops;
        }
        dbstops=dbstops.map(function(stop,i){
            stop.ests= this.ests[i].map(function(est){//[est.carid,est.est,est.countdowntime,est.writetime]
                return new Est(est,this.states);// {carid:est[0],est:est[1],countdowntime:est[2],writetime:est[3]};
            },self);
            if(schs)
            {
                if((index>0 && (schs[i]-Date.linux_now())<-300)||
                    (stop1[3] && schstate &&
                        ((goback==1 && schstate.goback==2) || (i<schstate.seqno))
                     )
                )
                {
                    stop.cometime='';
                    stop.comecarid='';
                    stop.schs=[];
                }else
                {
                    stop.cometime=schs[i]>0?new Date(schs[i]*1000).makeHM():"";
                    stop.comecarid=stop1[0];
                    stop1[2]=schs[i];
                    stop.schs=[new Sch(stop1)];
                }
            }else
            {
                stop.schs= this.schs[i].map(function(sch){//{"carid":"V0011","schid":"79339577","time":1417768689,"islast":true}
                    return new Sch(sch,this.states);// {carid:sch[0],schid:sch[1],time:sch[2],islast:sch[3]};
                },this);
                stop.comecarid=this.schs[i].length>0?this.schs[i][0][0]:"";
                stop.cometime=this.schs[i].length>0?(new Date(this.schs[i][0][2]*1000)).makeHM():"";
            }
            stop.value=this.value[i]>0?Math.ceil(this.value[i]/60):this.value[i];
            stop.carid=this.ests[i].length>0?this.ests[i][0][0] :"";
            return stop;
        },eststops);

        return dbstops;
    }
    tasks.nrun(function(){
        var fristSch=null;
        var index=0;
        if(Est_hander.sch_apply_frist_stop && eststops && eststops.schs.length>0)
        {
        //找到第一個有班表預估的站 && eststops.schs[0].length>0)//用首站的班表做為班表預估
            for(var i=0;i<eststops.schs.length;i++)
            {
                if(eststops.schs[i].length>0)
                {
                    fristSch=eststops.schs[i][0];
                    index=i;
                    break;
                }
            }
        }
        if(fristSch)
        {
            self.redis.send_command("get",["est:schedule.stats:"+fristSch[1]],function(err,res){

                var schstate=res?res.seval("est:schedule.stats:"+fristSch[1]):null;
                self.redis.send_command("hget",["bd:schedule:"+fristSch[1],'est'],function(err,res){
                    res=res.seval("bd:schedule:"+fristSch[1]);
                    var list=mager(dbstops,eststops,res?res[goback-1]:null,fristSch,index,schstate);
                    callback(list);
                });
            });

        }else{
            var list=mager(dbstops,eststops,null,null,index,null);
            callback(list);
        }

    });

};
Est_hander.prototype.queryLogicroute=function(routeno,callback){
    var tasks=new Tasks();
    var golist=[];
    var backlist=[];
    tasks.addHandler(this,this.queryRoute,[routeno,1,function(list){
        golist=list;
        tasks.next();
    }]);
    tasks.addHandler(this,this.queryRoute,[routeno,2,function(list){
        backlist=list;
        tasks.next();
    }]);
    tasks.nrun(function(){
        callback(golist,backlist);
    });
};
Est_hander.prototype.queryStop=function(routeno,goback,stopid,callback){
    this.queryRoute(routeno,goback,function(list){
        for(var i=0;i<list.length;i++){
            if(list[i].stopid==stopid)
            {
                callback(list[i]);
                return;
            }
        }
        callback(null);
    });
};
module.exports.Est_hander=Est_hander;