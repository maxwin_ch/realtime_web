var routes=[
    /*
   如果不需要再次路由，返回false
   如果需要路由，返回标准uri字串
   ctrl/method[.view]
    */
    function(uri,req){

        switch (uri){
            case '/getmstopest.json': return "multimediastop/getmstopest.json";break;
            case '/getmstopest.xml': return "multimediastop/getmstopest.xml"; break;
            case "/getbusdata.json": return  req.query.carids?"busdata/bycarids.json":(req.query.routeids?"busdata/byrouteids.json":"busdata/getall.json");break;
            case "/getbusdata.xml": return  req.query.carids?"busdata/bycarids.xml":(req.query.routeids?"busdata/byrouteids.xml":"busdata/getall.xml");break;
            case "/realtime/get_by_fleet":return "busdata/byfleet.json";break;
            case "/realtime/for_stop":return "busdata/for_stop.json";break;
            default :
                    return false;
        }

    },
    function(uri,req){
        var ss=uri.split('/');
        if(ss[0]=="") ss.shift();
       // req.query.lang="zh";
        if(ss.length==3 && !req.query.lang)
            req.query.lang=ss.pop();
        var view=ss.pop();
        view=view.split('.');
        var fun=view[0];
        view=view.length==2?view[1]:(req.query.view?req.query.view:"xml");
        if(ss[0]=="realtime" && fun=="getestimatetime")
            return  (req.query.routeids?"estimatetime/byrouteids."+view:"estimatetime/getall."+view);
        else
            return false;
    }
];
module.exports=function(req){
    var uri=req._parsedUrl.pathname,
    uri=uri.toLowerCase();
    for(var i=0;i<routes.length;i++){
        var re=(routes[i](uri,req));
        if(re!==false)
            return re;
    }
    return false;
}