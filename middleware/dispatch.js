/**
 * Created by hewin on 14-9-26.
 */
var CONTROLLER_PATH =__dirname+'/../controller/';
var fs=require('fs');
var Module=require("module");
var path = require('path');
var router=require('./router');
var controller_helpers={};
var os=require('os');
var onHeaders = require('on-headers');
module.exports.add_controller_helpers=function(name,helper){
    controller_helpers[name]=helper;
};
module.exports.dispatch=function(req, res, next) {
    var s=JSON.stringify({time:(new Date()).datetime2str(),method:req.method, uri:req.originalUrl,header:req.headers});
    console.log(s);

    onHeaders(res,function(){
        if (this.getHeader('x-by-host'))
            return;
        this.setHeader('x-by-host',os.hostname());
    });
    var app=req.app;
    var file=path.join(__dirname,req._parsedUrl.pathname);
    if (!fs.existsSync(file))
    {
        var keys=Object.keys(req.query);
        keys.forEach(function(key){
           req.query[key.toLowerCase()]=req.query[key];
        });
        var uri=router(req);
        uri=uri===false?req._parsedUrl.pathname:uri;
        var ps=uri.split('/');
        if(ps[0]=="") ps.shift();
        if(ps.length==2 && dispatch(ps[0],ps[1],app,req,res))
           return true;
        else
          return next();

    }else
        return next();

};

function dispatch(ctrl,method ,app,req,res){
    ctrl=ctrl.toLowerCase();
    methed=method?method.toLowerCase():'index';
    method=method.replace(".",'$');
    var jsfile=path.normalize(CONTROLLER_PATH+ctrl+'.js');
    var ctrl_unit=Module._cache[jsfile];
    if(!ctrl_unit)
        return false;
    var classname=ctrl.replace(/\b(\w)|\s(\w)/g,function(m){return m.toUpperCase()});
    var ctrlClass=ctrl_unit.exports[classname];
    if(typeof ctrlClass.prototype[method] =='undefined')
        return false;
    var ctrlobj=new ctrlClass();
    ctrlobj.req=req;
    ctrlobj.res=res;
    ctrlobj.app=app;
    if(typeof ctrlobj.init=='function')
        ctrlobj.init();
    var keys=Object.keys(controller_helpers);
    keys.forEach(function(key){
       ctrlobj[key]= controller_helpers[key];
    });
    //替換response.send
    var originalSend = res.send;
    var originalType=res.type;
    res.send = function() {
        if (this._header){
            console.error({time:(new Date()).datetime2str(),uri:req.url, header:this._header});
            return this;
        }
        return originalSend.apply(this, arguments)
    };
    res.type = function() {
        if (this._header){
            console.error({time:(new Date()).datetime2str(),uri:req.url, header:this._header});
            return this;
        }
        return originalType.apply(this, arguments)
    };
    ctrlobj[method].apply(ctrlobj);
    return true;
}
init_controllers=function(){
    var files=fs.readdirSync(CONTROLLER_PATH);
    files.forEach(function(file){
        file=path.join(CONTROLLER_PATH,file);
        file=path.normalize(file);
        if(file.substr(-3,3)=='.js')
            require(file);
    });
};
init_controllers();