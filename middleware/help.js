var CONTROLLER_PATH =__dirname+'/../controller/';
var fs=require('fs');
var router=require('./router');
var Tasks=require('maxwin_utils').Tasks;
var path = require('path');
var os=require('os');
function fundoc(fun){
    return fun.toString().replace(/^[^\/]+\/\*!?\s?/, '').replace(/\*\/[^\/]+$/, '');

}
module.exports=function(){
    return function(req, res) {
        var uri=req._parsedUrl.pathname.toLowerCase();
        res.writeHead
        var content="";
        res.set("Content-Type","text/html; charset=utf-8");
        var tasks=new Tasks();
        tasks.addHandler(this,fs.readFile,[path.join(__dirname,'../package.json'), function (err, data) {
            if (!err){
                var package=JSON.parse(data);
                if(package!=null){
                    var ver="version: "+package.version+"<br>";
                    ver+="host: "+os.hostname()+"<br>";
                    ver+="author: <a href=\"mailto:"+package.author.email+"\">"+package.author.name+"</a>";
                    content+="<script > document.getElementById('version').innerHTML='"+ver+"';</script>";
                }
            }
            tasks.next();
        }]);
        tasks.addHandler(null,function(){
            var files=fs.readdirSync(CONTROLLER_PATH);
            var index="<h1>Controller列表</h1><code><ul>";
            files.forEach(function(file){
                if(file.substr(-3,3)=='.js'){
                    file=file.substr(0,file.length-3);
                    var ctrlunit=require(CONTROLLER_PATH+file);
                    if(ctrlunit){
                        index+="<li><a href=\"#"+file+"\">"+file+"</a></li>";
                        var classname=file.replace(/\b(\w)|\s(\w)/g,function(m){return m.toUpperCase()});
                        if(typeof ctrlunit[classname]=="function"){
                            content+="<a name=\""+file+"\"><p><b>"+file+"</b><a href=\"#top\" class=\"top\">Top</a></p></a>";
                            content+=" <code><div>Method list</div><ul>";
                            var ctrlclass= ctrlunit[classname];
                            for(var p in ctrlclass.prototype){
                                if(typeof ctrlclass.prototype[p] =='function' && p.charAt(0)!='_'){
                                    content+= "<li>"+file+"\/"+ p.replace('$','.')+"</li>\n\r";
                                }
                            }
                            content+="</ul>";
                        }
                        if(ctrlunit && typeof ctrlunit.README=='function' )
                            content+="<div class=\"detail\"><hr>"+fundoc(ctrlunit.README)+"<\/div>";
                        content+="</code>";
                    }
                }
            });
            content=fundoc(help_head)+index+content+fundoc(help_tail);
            tasks.next();
        },[]);
        tasks.run(function(){
            res.end(content);
        })
    }
}
function help_head(){
    /*<!DOCTYPE html>
     <html lang="en">
     <head>
     <link rel="icon" href="favicon.ico" mce_href="favicon.ico" type="image/x-icon">
     <meta charset="utf-8">
     <title>WEB API</title>
     <style type="text/css">
     ::selection{ background-color: #E13300; color: white; }
     ::moz-selection{ background-color: #E13300; color: white; }
     ::webkit-selection{ background-color: #E13300; color: white; }
     body {
     background-color: #fff;
     margin: 40px;
     font: 13px/20px normal Helvetica, Arial, sans-serif;
     color: #4F5155;
     }
     a {
     color: #003399;
     background-color: transparent;
     font-weight: normal;
     }

     h1 {
     color: #444;
     background-color: transparent;
     border-bottom: 1px solid #D0D0D0;
     font-size: 19px;
     font-weight: normal;
     margin: 0 0 14px 0;
     padding: 14px 15px 10px 15px;
     }
     code {
     font-family: Consolas, Monaco, Courier New, Courier, monospace;
     font-size: 12px;
     background-color: #f5f5f5;
     border: 1px solid #D0D0D0;
     color: #002166;
     display: block;
     margin: 14px 0 14px 0;
     padding: 12px 10px 12px 10px;
     }

     #body{
     margin: 0 15px 0 15px;
     }

     p.footer{
     text-align: right;
     font-size: 11px;
     border-top: 1px solid #D0D0D0;
     line-height: 32px;
     padding: 10px 10px 10px 10px;
     margin: 20px 0 0 0;
     }

     .container{
     margin: 10px;
     border: 1px solid #D0D0D0;
     -webkit-box-shadow: 0 0 8px #D0D0D0;
     }
     .top{
     float:right;
     }
     .maintext{
     font-size: 11px;
     margin-left: 20px;
     line-height:20pt;
     }
     .expand{
     height:18px;
     cursor: hand;
     width:120px;
     margin-bottom: 5px;
     padding-left: 5px;
     font-family: Consolas, Monaco, Courier New, Courier, monospace;
     font-size: 12px;
     color: blue;

     }
     .detail{
     font-size: 12px;
     }
    #version{
     text-align: right;

    }
     </style>
     <script type="text/javascript" src="prototype.js"></script>
     </head>
     <body>
     <a name="top">
     <h1>Welcome to Maxwin web API</h1>
     </a>
    <div id="version"></div>
     <div class="container"/>

     <div id="body">


     */
}
function help_tail(){
    /*	</div>
     <hr>
     <div >
     <div style="text-align: right;padding-right: 20px"> <a href="http://www.maxwin.com.tw" target="_blank">www.maxwin.com.tw</a></div>
     <div style="text-align: right;padding-right: 20px" ><a href="mailto:hewin@maxwin.com.tw">hewin@maxwin.com.tw</a></div>
     </div>
     </div>
     <script>
     function doexpand(event){
     var btn=Event.element(event);
     if (btn.expanded){
     Element.hide(btn.detail);
     btn.expanded=false;
     btn.innerHTML='更多内容';
     }
     else{
     Element.show(btn.detail);
     btn.expanded=true;
     btn.innerHTML='收起详细';
     }
     }
     function loaded(){
     var es=document.getElementsByClassName('detail');
     for(var i=0;i<es.length;i++){
     var container=document.createElement('div');
     var btn=document.createElement('a');
     btn.href='javascript:void(0);';
     //Element.addClassName(btn,'expand');
     container.appendChild(btn);
     btn.innerHTML='更多内容';
     btn.detail=es[i];
     es[i].parentElement.appendChild(container);
     container.appendChild(es[i]);
     Element.hide(es[i]);
     btn.expanded=false;
     Event.observe(btn, 'click',doexpand);

     }
     }
     Event.observe(window, 'load',loaded);

     </script>
     </body>
     </html>

    */
}