
var compression = require('compression');
var express = require('express');
var responseTime = require('response-time');
var cookieParser = require('cookie-parser');
var maxwin_utils=require("maxwin_utils");
var fs=require('fs');
function main(){

    if(maxwin_utils.configfilename()==null){
        console.error("not find configfile.");
        return false;
    }
    else{
        var config =require(maxwin_utils.configfilename());
        if(!config.redis){
            console.error("not find redis in configfile.");
            return ;
        }
        if(!config.pubsub){
            console.error("not find pubsub in configfile.");
            return ;
        }
        var app = express();
        app.set("config",config);
        app.enable('trust proxy');
        var help=require('./middleware/help');
        var Redis=require('redis');
        var path=require("path");
        var Est_hander=require('./middleware/est_hander').Est_hander;
        Est_hander.sch_apply_frist_stop=config.sch_apply_frist_stop?config.sch_apply_frist_stop:false;
    //var session = require('express-session');
    //var RedisStore = require('connect-redis')(session);
        var Dispatch =require('./middleware/dispatch');
        app.listen(config.web_port);
        console.log((new Date).datetime2str(),config);
        app.use(cookieParser());
        app.use(responseTime());
        app.set('etag',false);//动态数据变动频繁，不需要etag
        app.disable('x-powered-by');
        app.redis=Redis.createClient(config.redis.port,config.redis.host,{socket_keepalive:false,retry_delay:5000});
        app.rediswriter=Redis.createClient(config.rediswriter.port,config.rediswriter.host,{socket_keepalive:false,retry_delay:5000});
        app.pubsub=Redis.createClient(config.pubsub.port,config.pubsub.host);
        app.publish=Redis.createClient(config.pubsub.port,config.pubsub.host);

        app.ext_hander= new Est_hander(app.redis,app.pubsub);
        Dispatch.add_controller_helpers('redis',app.redis);
        app.use(compression());
        app.get('/help',help());
        app.use(Dispatch.dispatch);
        var webroot=config.web_root?config.web_root:"public";
        //module.load
        app.use(express.static(webroot));
    }
}
main();